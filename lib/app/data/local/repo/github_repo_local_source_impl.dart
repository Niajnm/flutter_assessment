import 'package:flutter_assessment/app/core/services/service_locator.dart';
import 'package:flutter_assessment/app/data/local/db/constant.dart';
import 'package:flutter_assessment/app/data/local/db/database_helper.dart';
import 'package:flutter_assessment/app/data/local/repo/model/repository_entity.dart';
import 'package:flutter_assessment/app/data/remote/github%20repo/github_repo_remote_source.dart';
import 'package:flutter_assessment/app/data/remote/github%20repo/model/github_repository_params.dart';
import 'package:flutter_assessment/app/utils/constants.dart';
import 'package:sqflite/sqflite.dart';

import 'github_repo_local_source.dart';

class GithubRepoLocalSourceImpl implements GithubRepoLocalSource {
  DatabaseHelper databaseHelper; // new property

  GithubRepoLocalSourceImpl({required this.databaseHelper});

  @override
  Future<List<RepositoryEntity>> getRepositoryList(
      GitHubRepositoryParams params) async {
    var db = await databaseHelper.database;
    int limit = 10;
    int offSet = (params.pageNo - 1) * limit + 1;
    String orderBy = "stars";
    if (params.sortBy == sortByStars) {
      orderBy = REPO_ID;
    } else {
      orderBy = REPO_COLUMN_UPDATED_AT;
    }

    List<Map<String, dynamic>> repoMaps = await db.query(
      REPO_TABLE,
      limit: limit,
      offset: offSet,
      orderBy: '$orderBy ASC',
    );
    return repoMaps.map((map) => RepositoryEntity.fromMap(map)).toList();
  }

  @override
  Future<void> insertRepositoryList(List<RepositoryEntity> repoList) async {
    var db = await databaseHelper.database;
    for (var repo in repoList) {
      await db.insert(REPO_TABLE, repo.toMap());
    }
  }
}
