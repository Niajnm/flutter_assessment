import 'package:flutter_assessment/app/data/remote/github%20repo/model/github_repository_params.dart';

import 'model/repository_entity.dart';

abstract class GithubRepoLocalSource {
  Future<void> insertRepositoryList(List<RepositoryEntity> repo);
  Future<List<RepositoryEntity>> getRepositoryList(
      GitHubRepositoryParams params);
}
