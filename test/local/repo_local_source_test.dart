import 'package:flutter_assessment/app/data/local/db/constant.dart';
import 'package:flutter_assessment/app/data/local/db/database_helper.dart';
import 'package:flutter_assessment/app/data/local/repo/github_repo_local_source_impl.dart';
import 'package:flutter_assessment/app/data/local/repo/model/repository_entity.dart';
import 'package:flutter_assessment/app/data/remote/github%20repo/model/github_repository_params.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';
import 'local_source_test.mocks.dart';

@GenerateNiceMocks([MockSpec<DatabaseHelper>()])
void main() {
  late GithubRepoLocalSourceImpl localSource;
  late MockDatabaseHelper mockDatabaseHelper;
  RepositoryEntity repo = RepositoryEntity(
      id: 1,
      description: "Flutter repo",
      name: "Usman",
      fullName: "Usman ALi",
      login: "John",
      imageUrl: "www.flaticon.com",
      language: "Dart",
      updatedAt: "10:20:60");

  List<RepositoryEntity> repoList = List.generate(10, (index) => repo);
  setUp(() {
    mockDatabaseHelper = MockDatabaseHelper();
    localSource = GithubRepoLocalSourceImpl(databaseHelper: mockDatabaseHelper);
  });

  test('getRepositoryList should return a list of RepositoryEntity', () async {
    // Mock the behavior of the DatabaseHelper.database method
    final mockDb = MockDatabase();
    when(mockDatabaseHelper.database).thenAnswer((_) async => mockDb);

    // Mock the behavior of the Database.query method
    final mockRepoMaps = [
      {
        'id': 1,
        'name': 'name',
        'full_name': 'fullName',
        'login': 'login',
        'image_url': 'imageUrl',
        'description': 'description',
        'updated_at': '2021-11-11T11:42:31Z',
        'language': 'language'
      },
      {
        'id': 2,
        'name': 'name',
        'full_name': 'fullName',
        'login': 'login',
        'image_url': 'imageUrl',
        'description': 'description',
        'updated_at': '2021-11-11T11:42:31Z',
        'language': 'language'
      }
      // Add more mock data as needed
    ];
    when(mockDb.query(
      any,
      limit: anyNamed('limit'),
      offset: anyNamed('offset'),
      orderBy: anyNamed('orderBy'),
    )).thenAnswer((_) async => mockRepoMaps);

    final params = GitHubRepositoryParams(pageNo: 1, sortBy: 'stars');
    final result = await localSource.getRepositoryList(params);

    // Verify that the DatabaseHelper.database method was called
    verify(mockDatabaseHelper.database);

    // Verify that the Database.query method was called with the expected parameters
    verify(mockDb.query(
      any,
      limit: 10,
      offset: 1,
      orderBy: 'id ASC',
    ));

    // Verify that the result is a list of RepositoryEntity
    expect(result, isA<List<RepositoryEntity>>());
  });

  test('insertRepositoryList should call insert method with correct parameters',
      () async {
    // Mock the behavior of the DatabaseHelper.database method
    final mockDb = MockDatabase();
    when(mockDatabaseHelper.database).thenAnswer((_) async => mockDb);

    // Mock the behavior of the Database.insert method
    when(mockDb.insert(
      any,
      any,
    )).thenAnswer((_) async => 1); // Assuming the insert is successful

    // Call the method under test
    await localSource.insertRepositoryList(repoList);

    // Verify that the DatabaseHelper.database method was called
    verify(mockDatabaseHelper.database);

    // Verify that the Database.insert method was called for each repository
    verify(mockDb.insert(
      REPO_TABLE,
      any,
    )).called(repoList.length);
  });
}
