import 'package:flutter_assessment/app/data/local/db/constant.dart';
import 'package:flutter_assessment/app/data/local/db/database_helper.dart';
import 'package:flutter_assessment/app/data/local/repo/github_repo_local_source_impl.dart';
import 'package:flutter_assessment/app/data/local/repo/model/repository_entity.dart';
import 'package:flutter_assessment/app/data/remote/github%20repo/model/github_repository_params.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

import 'local_source_test.mocks.dart';

@GenerateNiceMocks([MockSpec<DatabaseHelper>(), MockSpec<Database>()])
void main() {
  late Database database;
  late MockDatabaseHelper mockDatabaseHelper;
  late MockDatabase mockDatabase;
  RepositoryEntity repo = RepositoryEntity(
      id: 1,
      description: "Flutter repo",
      name: "Usman",
      fullName: "Usman ALi",
      login: "John",
      imageUrl: "www.flaticon.com",
      language: "Dart",
      updatedAt: "10:20:60");

  setUpAll(() async {
    sqfliteFfiInit();
    databaseFactory = databaseFactoryFfi;
    database = await databaseFactoryFfi.openDatabase(inMemoryDatabasePath);
    await database.execute(
      'CREATE TABLE $REPO_TABLE($REPO_ID INTEGER PRIMARY KEY AUTOINCREMENT, '
      '$REPO_COLUMN_NAME TEXT, '
      '$REPO_COLUMN_FULL_NAME TEXT, '
      '$REPO_COLUMN_OWNER_LOGIN TEXT, '
      '$REPO_COLUMN_OWNER_IMAGE_URL TEXT, '
      '$REPO_COLUMN_DESCRIPTION TEXT, '
      '$REPO_COLUMN_LANGUAGE TEXT, '
      '$REPO_COLUMN_UPDATED_AT TEXT)',
    );

    mockDatabaseHelper = MockDatabaseHelper();
    var mockDb = await mockDatabaseHelper.database;
    mockDb = database;
    mockDatabase = MockDatabase();
  });

  group('Database test', () {
    test('sqflite version', () async {
      expect(await database.getVersion(), 0);
    });

    test('add Item to database', () async {
      var i = await database.insert(REPO_TABLE, repo.toMap());
      var p = await database.query(REPO_TABLE);
      expect(p.length, i);
    });
  });
}
