import 'package:flutter_assessment/app/data/local/repo/github_repo_local_source.dart';
import 'package:flutter_assessment/app/data/local/repo/model/repository_entity.dart';
import 'package:flutter_assessment/app/data/remote/github%20repo/github_repo_remote_source.dart';
import 'package:flutter_assessment/app/data/remote/github%20repo/model/github_owner.dart';
import 'package:flutter_assessment/app/data/remote/github%20repo/model/github_repository.dart';
import 'package:flutter_assessment/app/data/remote/github%20repo/model/github_repository_params.dart';
import 'package:flutter_assessment/app/data/remote/github%20repo/model/github_repository_response.dart';
import 'package:flutter_assessment/app/data/repository/repo/github_repository_impl.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'repository_test.mocks.dart';

@GenerateNiceMocks([
  MockSpec<GithubRepoLocalSource>(),
  MockSpec<GitHubRepositoryRemoteSource>()
])
void main() {
  late GitHubRepositoryImpl repository;
  late MockGithubRepoLocalSource mockLocalSource;
  late MockGitHubRepositoryRemoteSource mockRemoteSource;

  setUp(() {
    mockLocalSource = MockGithubRepoLocalSource();
    mockRemoteSource = MockGitHubRepositoryRemoteSource();
    repository = GitHubRepositoryImpl(
        localSource: mockLocalSource, remoteSource: mockRemoteSource);
  });

  test('getRepositoryList returns local data if available', () async {
    // Mock the behavior of localSource.getRepositoryList to return non-empty data
    final params = GitHubRepositoryParams(sortBy: "stars", pageNo: 1);
    final localData = [
      RepositoryEntity(
          id: 1,
          description: "Flutter repo",
          name: "Usman",
          fullName: "Usman ALi",
          login: "John",
          imageUrl: "www.flaticon.com",
          language: "Dart",
          updatedAt: "10:20:60"),
      RepositoryEntity(
          id: 1,
          description: "Flutter repo",
          name: "Usman",
          fullName: "Usman ALi",
          login: "John",
          imageUrl: "www.flaticon.com",
          language: "Dart",
          updatedAt: "10:20:60"),
    ];
    when(mockLocalSource.getRepositoryList(params))
        .thenAnswer((_) async => localData);

    // Call the method under test
    final result = await repository.getRepositoryList(params);

    // Verify that remoteSource.getRepositoryList is not called
    verifyNever(mockRemoteSource.getRepositoryList(params));

    // Verify that localSource.getRepositoryList is called
    verify(mockLocalSource.getRepositoryList(params)).called(1);

    // Verify that the result matches the local data
    expect(result, localData);
  });
}
